import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import Item from '../models/item';


// build Type Composer with model and options
const customizationOptions = {};
const ItemTC = composeWithMongoose(Item, customizationOptions);

/*
CollectionTC.addFields({
	itemsCount: {
		type: 'Int',
		description: 'Get count of items in collection',
		resolve: (source, args, context, info) => 'some value',
	},
});
*/

/*
UserTC.addFields({
    lonLat: TypeComposer.create('type LonLat { lon: Float, lat: Float }'),
    notice: 'String', // shorthand definition
    noticeList: { // extended
      type: '[String]', // String, Int, Float, Boolean, ID, Json
      description: 'Array of notices',
      resolve: (source, args, context, info) => 'some value',
    },
    bio: {
      type: GraphQLString,
      description: 'Providing vanilla GraphQL type'
    }
  })
*/


// exports
export default ItemTC;


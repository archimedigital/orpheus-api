// import accessControl from './access-control'; // authentication & authorization

import ArchiveTC from '../typecomposers/archive';

const archiveQuery = {
	archiveMany: ArchiveTC.getResolver('findMany'),
	archiveOne: ArchiveTC.getResolver('findOne'),
};

/*
const userMutation = {
	// role order matters, check role access from left to right
	...accessControl(['owner', 'admin'], {
		userUpdateById: UserTC.getResolver('updateById'),
	}),
	...accessControl(['admin'], {
		userCreateOne: UserTC.getResolver('createOne'),
		userCreateMany: UserTC.getResolver('createMany'),
		userUpdateOne: UserTC.getResolver('updateOne'),
		userUpdateMany: UserTC.getResolver('updateMany'),
		userRemoveById: UserTC.getResolver('removeById'),
		userRemoveOne: UserTC.getResolver('removeOne'),
		userRemoveMany: UserTC.getResolver('removeMany'),
	}),
}
*/

export {
	archiveQuery,
	//userMutation 
};

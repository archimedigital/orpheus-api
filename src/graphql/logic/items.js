import _s from 'underscore.string';
import shortid from 'shortid';
import rp from 'request-promise';
import request from 'request';
import axios from 'axios';


// services
import PermissionsService from './PermissionsService';

// models
import Item from '../../models/item';
import File from '../../models/file';
import Manifest from '../../models/manifest';
import Project from '../../models/project';

// errors
import { AuthenticationError, PermissionError, ArgumentError } from '../errors';

// AI related
import generateColorMeta from '../../lib/generateColorMeta';
import concatItemColorMetaDeDup from '../../lib/concatItemColorMetaDeDup';

// todo: move this lib to a proper directory
import getImageGoogleCloudVisionData from '../../modules/hul/lib/getImageGoogleCloudVisionData';
import cloudVisionDataToTags from '../../modules/hul/lib/cloudVisionDataToTags';
import getIiifUrl from '../../lib/getIiifUrl';


const saveFiles = async (project, item, files) => {

	const existingFiles = await File.find({
		itemId: item._id
	});

	// remove all existing files
	existingFiles.forEach(async (existingFile) => {
		await File.remove({
			_id: existingFile._id,
		});
	});

	// temp - items for some hostnames are by default using AI
	const listHostnameWithAI = process.env.AI_ENABLED_HOSTNAMES;
	const flagAiEnabled = listHostnameWithAI.includes(project.hostname);

	files.forEach(async (file) => {
		if (!('_id' in file)) {
			file._id = shortid.generate();
		}

		// relationships
		file.itemId = item._id;
		file.projectId = project._id;

		if (item.shouldUseMachineLearning || flagAiEnabled) {
			try {
				file.cloudVisionDataRaw = await getImageGoogleCloudVisionData(getIiifUrl(file)); // eslint-disable-line

				// generate tags from AI results
				// get top 3 colors
				const colors = file.cloudVisionDataRaw.imageProperties.imagePropertiesAnnotation.dominantColors.colors;
				const topColors = colors.slice(0, 3);

				// generate labels from raw color objects
				const listColorMetaJSON = generateColorMeta(topColors);
				const listColorMeta = listColorMetaJSON.map(colorMetaJSON => ({
					type: 'color',
					value: colorMetaJSON.RGB,
					valueJSON: colorMetaJSON,
					label: 'Color'
				}));

				// add colors to item meta
				const _itemMeta = concatItemColorMetaDeDup(item.metadata, listColorMeta);

				// append tags to item
				const labels = cloudVisionDataToTags(file.cloudVisionDataRaw);
				const _itemTags = labels.concat(item.tags);

				await Item.update({ _id: item._id, }, {
					$set: {
						tags: _itemTags,
						metadata: _itemMeta,
					},
				});

			} catch (err) {
				console.error(err);
			}
		}

		const newFile = new File(file);
		await newFile.save();
	});
};


const saveManifest = async (project, item, files) => {
	const images = [];
	files.forEach((file) => {
		if (file.type && file.type.startsWith('image')) {
			let newImageName = file.name;
			newImageName = newImageName.replace(`${file._id}-`, '');

			images.push({
				_id: file._id,
				name: newImageName,
				label: file.title,
			});
		}
	});

	if (!images.length) {
		return null;
	}

	// update item manifest
	const manifest = {
		itemId: item._id,
		title: item.title,
		label: item.title,
		description: item.description || '',
		attribution: project.title,
		images,
	};

	let existingManifest = await Manifest.findOne({ itemId: manifest.itemId });
	if (!existingManifest) {
		existingManifest = new Manifest(manifest);
		await existingManifest.save();
		existingManifest = await Manifest.findOne({ itemId: manifest.itemId });
	} else {
		await Manifest.update({
			itemId: manifest.itemId,
		}, {
			$set: manifest
		});
	}

	manifest._id = existingManifest._id;

	try {
		const manifestCreationResult = await axios.post(
			process.env.MANIFEST_REQUEST_URL,
			{
				manifest: JSON.stringify(manifest),
				responseUrl: process.env.MANIFEST_RESPONSE_URL,
			},
		);
	} catch (error) {
		console.log('Manifest creation error ... ', error);
	}


};


/**
 * Logic-layer service for dealing with items
 */

export default class ItemService extends PermissionsService {
	/**
	 * Count items
	 * @param {string} projectId
	 * @param {string} collectionId
	 * @returns {number} count of items
	 */
	async count({ projectId, collectionId, textsearch, filter, tags }) {
		const where = {};

		if (!projectId && !collectionId) {
			return 0;
		}

		if (projectId) {
			where.projectId = projectId;
		}

		if (collectionId) {
			where.collectionId = collectionId;
		}

		if (textsearch) {
			where.$text = { $search: textsearch };
		}

		if (filter) {
			where.$and = [];
			filter.forEach((filterParam) => {
				where.$and.push({
					metadata: {
						$elemMatch: {
							label: filterParam.name,
							value: { $in: filterParam.values },
						},
					},
				});
			});
		}

		if (tags) {
			where.tags = { $in: tags };
		}

		return await Item.count(where);
	}

	/**
	 * Get a list of items
	 * @param {string} projectId
	 * @param {string} collectionId
	 * @param {string} textsearch
	 * @param {object} filter
	 * @param {number} offset
	 * @param {number} limit
	 * @returns {Object[]} array of items
	 */
	async getItems({ projectId, collectionId, textsearch, filter, dateSearch, tags, ids, offset, limit }) {
		const args = {};

		if (!projectId && !collectionId) {
			return [];
		}

		if (projectId) {
			args.projectId = projectId;
		}

		if (collectionId) {
			args.collectionId = collectionId;
		}

		if (ids) {
			args._id = ids;
		}

		if (textsearch) {
			args.$text = { $search: textsearch };
		}

		if (filter) {
			args.$and = [];
			filter.forEach((filterParam) => {
				args.$and.push({
					metadata: {
						$elemMatch: {
							label: filterParam.name,
							value: { $in: filterParam.values },
						},
					},
				});
			});
		}

		// build filter for array of date ranges
		if (dateSearch) {
			// init mongo and structure
			if (!args.$and) {
				args.$and = [];
			}
			// build each date range filter
			dateSearch.map((dateInput) => {
				// set date range
				const dateRange = {};
				if (dateInput.dateStart) {
					dateRange.$gte = dateInput.dateStart;
				}
				if (dateInput.dateEnd) {
					dateRange.$lte = dateInput.dateEnd;
				}
				// build mongo filter
				args.$and.push({
					metadata: {
						$elemMatch: {
							label: dateInput.dateFieldLabel,
							value: { ...dateRange },
						},
					},
				});
			});
		}

		if (tags) {
			args.tags = { $in: tags };
		}

		return await Item.find(args)
			.sort({ slug: 1})
			.skip(offset)
			.limit(limit);
	}

	/**
	 * Get item
	 * @param {string} collectionId - id of collection of item
	 * @param {number} _id - id of item
	 * @param {string} slug - slug of item
	 * @returns {Object[]} array of items
	 */
	async getItem({ collectionId, _id, slug }) {
		const where = {};

		if (!_id && !slug) {
			return null;
		}

		if (_id) {
			where._id = _id;
		}

		if (slug) {
			where.slug = slug;
		}

		return await Item.findOne(where);
	}

	/**
	 * Create a new item
	 * @param {Object} item - item candidate
	 * @param {string} hostname - hostname of item project
	 * @param {[Object]} files - files for the object
	 * @returns {Object} created item
	 */
	async create(hostname, item, files) {
		// if user is not logged in
		if (!this.userId) throw new AuthenticationError();

		// find project
		const project = await Project.findOne({ hostname });
		if (!project) throw new ArgumentError({ data: { field: 'hostname' } });

		// validate permissions
		const userIsAdmin = this.userIsProjectAdmin(project);
		if (!userIsAdmin) throw new PermissionError();

		// Initiate new item
		item.projectId = project._id;
		item.slug = _s.slugify(item.title);
		const newItem = new Item(item);

		await newItem.save();

		if (files && files.length) {
			await saveFiles(project, newItem, files);
			await saveManifest(project, newItem, files);
		}

		// return new item
		return newItem;
	}

	/**
	 * Update a item
	 * @param {Object} item - item candidate
	 * @returns {Object} updated item
	 */
	async update(item, files) {
		// if user is not logged in
		if (!this.userId) throw new AuthenticationError();

		// find project
		const project = await Project.findOne({ _id: item.projectId });
		if (!project) throw new ArgumentError({ data: { field: 'item.projectId' } });

		// validate permissions
		const userIsAdmin = this.userIsProjectAdmin(project);
		if (!userIsAdmin) throw new PermissionError();

		// perform action
		const result = await Item.update({ _id: item._id }, { $set: item });
		const updatedItem = await Item.findById(item._id);

		// save files and add ids to item
		if (files && files.length) {
			await saveFiles(project, updatedItem, files);
			await saveManifest(project, updatedItem, files);
		} else {
			await saveFiles(project, updatedItem, []);
			await saveManifest(project, updatedItem, []);
		}

		// temp - items for some hostnames are by default using AI
		const listHostnameWithAI = process.env.AI_ENABLED_HOSTNAMES;
		const flagAiEnabled = listHostnameWithAI.includes(project.hostname);

		// TODO
		// error handling

		// return updated item
		return updatedItem;
	}

	/**
	 * Remove a item
	 * @param {string} _id - id of item to Remove
	 * @param {string} hostname - hostname of project to check permissions against
	 * @returns {boolean} remove result
	 */
	async remove(_id, hostname) {
		// if user is not logged in
		if (!this.userId) throw new AuthenticationError();

		// find project
		const project = await Project.findOne({ hostname });
		if (!project) throw new ArgumentError({ data: { field: 'hostname' } });

		// validate permissions
		const userIsAdmin = this.userIsProjectAdmin(project);
		if (!userIsAdmin) throw new PermissionError();

		// perform action
		const result = await Item.remove({ _id });

		// TODO
		// error handling

		// respond with result
		return {
			result,
		};
	}

	/**
	 * Get item activity feed
	 * @param {number} itemId - item id for activity
	 * @param {number} limit - mongoose orm limit
	 * @param {number} offset - mongoose orm offset
	 * @returns {Object[]} activity feed items
	 */
	async getActivityFeed({ itemId, limit, offset }) {

		// TODO:
		// get activity feed from items, items, articles, texts, and comments

		return [];
	}
}

/**
 * generates iiif url
 * 
 */


// todo: make iiif url env var; make image size parameter;
const getIiifUrl = file => `https://iiif.orphe.us/${file.name}/full/full/0/default.jpg`;


export default getIiifUrl;
